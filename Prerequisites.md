# Prerequisites

To be successful with this kata, you, the developer(s), should be prepared to take control of a computer running an instance of IntelliJ IDEA (Community Edition or better).

The facilitator has set up Zoom (or equivalent) so that you can have this control as the current "driver" when it is your turn.

When you are assigned control of the mouse and keyboard the first thing you want to do is to ensure that your keymap is appropriate for your computer operating system (OS).

 | OS | Keymap |
 |----|--------|
 |Windows | Visual Studio or IntelliJ IDEA Classic |
 |Mac | macos |
 |Linux | GNOME or KDE or XWin |

You should be familiar with Git and the IntelliJ IDEA Git menu and/or shortcuts.

Basic understanding of GitHub or GitLab flow and an account on GitLab is also required.

If you feel weak in any of these areas, reach out to one of the facilitators or the organizer for some help.

